/* eslint no-console: "off" */
const webpack = require('webpack');
const path = require('path');
const fs = require('fs');
const assert = require('assert');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');

const { title } = JSON.parse(fs.readFileSync('./package.json'));
assert(title, 'No title specified in package.json');

const { BUILD_ASSET_URL = '' } = process.env;

const config = {
  entry: {
    index: './src/index.jsx'
  },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: '[name].[hash].js',
    sourceMapFilename: '[name].[hash].map',
    publicPath: `${BUILD_ASSET_URL}/`
  },
  plugins: [
    new webpack.DefinePlugin({ BASE_PATH: JSON.stringify('/') }),

    new CleanWebpackPlugin(),

    new HtmlWebpackPlugin({
      title,
      template: path.join(__dirname, 'src', 'index.ejs')
    })
  ],
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          presets: [
            '@babel/preset-react',
            [
              '@babel/preset-env',
              {'targets': {'esmodules': true} }
            ]
          ],
          plugins: ['@babel/plugin-syntax-dynamic-import', '@babel/plugin-proposal-class-properties' ]
        }
      }
    ]
  },

  resolve: {
    extensions: ['.js', '.jsx']
  }
};

module.exports = config;
