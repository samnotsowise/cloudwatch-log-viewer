const merge = require('webpack-merge');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');

const prod = require('./webpack.prod.js');

module.exports = merge(prod, {
  mode: 'production',
  optimization: { minimize: true, concatenateModules: false },
  plugins: [
    new BundleAnalyzerPlugin({
      analyzerMode: 'static',
      openAnalyzer: true,
      generateStatsFile: false
    })
  ]
});
