import React, {useState} from 'react';
import {
  Segment,
  Grid
} from 'semantic-ui-react';
import ExistingConnections from './ExistingConnections';
import NewConnection from './NewConnection';

export default function Connect({establishConnection}) {

  const defaultConnections = window.localStorage.connections && JSON.parse(window.localStorage.connections) || [];
  const [connections, setConnectionsState] = useState(defaultConnections);

  const setConnections = (newConnections) => {
    setConnectionsState(newConnections);
    window.localStorage.connections = JSON.stringify(newConnections);
  };

  return (
    <Segment placeholder basic style={{minHeight: '100vh'}}>
      <Grid stackable centered>
        <Grid.Row>
          <Grid.Column mobile={16} tablet={8} computer={4}>
            <ExistingConnections
              connections={connections}
              connect={establishConnection}
              setConnections={setConnections}
            />
          </Grid.Column>
          <Grid.Column mobile={16} tablet={8} computer={4}>
            <NewConnection
              connect={establishConnection}
              addConnection={(connection) => setConnections([...connections, connection])}
            />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Segment>
  );
};
