import React from 'react';
import {Header, Icon, List, Segment} from 'semantic-ui-react';
import ExistingConnection from './ExistingConnection';

export default function ExistingConnections({connect, connections, setConnections}) {
  if (connections.length === 0) {
    return (
      <Segment placeholder>
        <Header icon>
          <Icon name='sitemap' />
          No Saved Connections
        </Header>
        <p>Save yourself some time - store connection details in local storage</p>
      </Segment>
    );
  }
  return (
    <List selection>
      <List.Header>Stored Connections</List.Header>
      {connections.map(connection => <ExistingConnection
        key={connection.key}
        connect={connect}
        connection={connection}
        deleteConnection={({key}) => setConnections(connections.filter(({key: cKey}) => cKey !== key))}
      />)}
    </List>
  );
}