import React, {useState} from 'react';
import {Button, List} from 'semantic-ui-react';

export default function ExistingConnection({connection, connect, deleteConnection}) {
  const {name, key} = connection;
  const [deleting, setDeleting] = useState(false);

  return (
    <List.Item key={key} onClick={() => connect(connection)}>
      <List.Content floated='right'>
        <Button
          icon="trash"
          size="tiny"
          onClick={(event) => {
            event.stopPropagation();
            if (deleting) {
              deleteConnection(connection);
            } else {
              setDeleting(true);
            }
          }}
          negative={deleting} />
      </List.Content>
      <List.Icon name='computer' size='large' verticalAlign='middle' />
      <List.Content>
        <List.Header>{name}</List.Header>
        <List.Description>{key}</List.Description>
      </List.Content>
    </List.Item>
  );
}