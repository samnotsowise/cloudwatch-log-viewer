import React, {useState} from 'react';
import {Form} from 'semantic-ui-react';

export default function NewConnection({connect, addConnection}) {
  const [key, setKey] = useState('');
  const [secret, setSecret] = useState('');
  const [name, setName] = useState('');
  const [remember, setRemember] = useState(false);

  const handleChange = (stateSetter, valueProp = 'value') => (event, props) => {
    stateSetter(props[valueProp]);
  };

  return (
    <Form>
      <Form.Input
        fluid
        placeholder="required"
        label="Access Key"
        value={key}
        onChange={handleChange(setKey)} />
      <Form.Input
        fluid
        type="password"
        placeholder="required"
        label="Access Secret"
        value={secret}
        onChange={handleChange(setSecret)} />
      <Form.Input
        fluid
        label="Name"
        placeholder="New Connection"
        value={name}
        onChange={handleChange(setName)} />
      <Form.Checkbox
        label="Save This Connection"
        checked={remember}
        onChange={handleChange(setRemember, 'checked')} />
      <Form.Button
        disabled={!(key && secret)}
        onClick={() => {
          connect({key, secret, name});
          if (remember) {
            addConnection({key, secret, name: name || 'New Connection'});
          }
        }}
        fluid
        primary
      >
        {remember && 'Save & '}
        Connect
      </Form.Button>
    </Form>
  );
}
