import React, {useState} from 'react';
import {Redirect, Switch} from 'react-router';
import {BrowserRouter, Route} from 'react-router-dom';
import Connect from './Connect';

export default function Main() {

  const [connection, setConnection] = useState();

  const establishConnection = ({key, secret, name = 'New Connection'}) => console.log('connecting', key, secret, name);

  return (
    <BrowserRouter basename="/">
      <Switch>
        <Route path="/" render={() => <Connect establishConnection={establishConnection} />} />
        {(connection &&
          <Route render={() => <Redirect to="/" />} />) || <Route render={() => <Redirect to="/" />} />}
      </Switch>
    </BrowserRouter>
  );
}
